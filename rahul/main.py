"""
Rahul Rangarajan 10/13/2020
"""

from PIL import Image
from PIL import ImageEnhance
from PIL import ImageOps
from PIL import ImageDraw
from PIL import ImageFont
from PIL import ImageFilter

def main() :
    print("This is BuildAPic")
    #open the images I will be using
    image1 = Image.open("used images/water3.jpg")
    image2 = Image.open("used images/Leia.jpg")
    image3 = Image.open("used images/FFXIV.png")


    finish = cropImage(image3, 300, 200, image3.width - 500, image3.height - 100)
    finish = sharpenImage(finish, 200)
    finish.save("FFXIV New.png")

#main()

def greyscaleImage(image):
    print("greyscale")
    #changes the mode of image from RGB to greyscale
    image = image.convert("L")

    return image

#greyscale()

def cropImage(image1, left, top, right, bottom):
    print("cropping")
    #crops image to the set pixels
    croppedImage = image1.crop((left, top, right, bottom))

    return croppedImage

#crop()

def sharpenImage(image1, factor):
    print("Sharpness")
    #create enhancer which will be the object that gets sharpened to a factor. 0 retruns original image while increasing the factor adds more color.
    # Code From the PILLOW Module
    enhancer = ImageEnhance.Sharpness(image1)
    image1 = enhancer.enhance(factor).show(f"Sharpness{factor:f}")

    return image1
#sharpenImage()

def invertion(image):
    print("invert colors")

    #ensures that image is in RGB mode
    image.convert("RGB")

    #returns the inverted version of image
    return ImageOps.invert(image)

def imageFilter(image1, n):
    print("imageFilter")
    #blurs an image to the factor of n
    filtered = image1.filter(ImageFilter.BoxBlur(n))
    return filtered

#imageFilter()

def drawCircle(image1):
    print("drawing circle")
    #creates a elipse with bounds at (x1, y1, x2, y2) which is a touple of specific pixels
    #Code From PILLOW module
    draw = ImageDraw.Draw(image1)
    draw.ellipse((0, image1.height-200, 400, image1.height), fill=None, width=10)
    return image1

#drawCircle()

def watermark(image):
    print("making watermark")
    #creates font variable containing the font arial
    fontImage = ImageFont.truetype("arial.ttf", 28)
    # Code from Stack with some adjustments made by me
    d = ImageDraw.Draw(image)
    #creates a line of text of color (255, 255, 0) at location 50, height -125
    d.multiline_text((50, image.height-125), "This is property of Rahul", font=fontImage, fill=(256, 256, 0))
    return image

#watermark()

if __name__ == "__main__":
    main()