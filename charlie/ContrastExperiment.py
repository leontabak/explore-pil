# Charlie Honigman
# CSC 355
# 10/13/2020
# Week 1 Day 2 Program

# Goals:
# I wanted to play around with the ImageEnhance module
# and see if I could replicate some of the effects by manipulating
# a numpy array. I found that the ImageEnhance module offers more
# control over changes than directly manipulating the numpy array.


import numpy as np
from PIL import Image, ImageEnhance

def main():
    image = Image.open("images/WRXProfile.jpg")
    image = image.rotate(270)

    # ImageEnhance method
    # An ImageEnhance object can modify many aspects of an image.
    # You have to use the desired method(contrast, brightness, etc)
    # Before actually providing factors of modification
    # I think of it as your object being in a specific mode
    # The methods that put your ImageEnhance object into a certain
    # "mode" usually just require an image as its only parameter
    enhancer = ImageEnhance.Contrast(image)

    # Now we can tell the ImageEnhance object the factor that we want our
    # attribute to change by
    highCon = enhancer.enhance(2)
    highCon.show()

    lowCon = enhancer.enhance(0.75)
    lowCon.show()

    # numpy approach
    # This approach starts by converting the image to a 3D array
    data = np.asarray(image)
    satData = data.copy()

    # Then you apply some transformation to each element
    # I found that, when applying these transformations, you
    # Must multiply or divide by an integer
    highData = satData * 2
    lowData = satData // 2

    # Finally, you convert back to an image
    highImage = Image.fromarray(highData, "RGB")
    highImage.show()

    lowImage = Image.fromarray(lowData, "RGB")
    lowImage.show()


if __name__ == "__main__":
    main()