# Charlie Honigman
# CSC 355
# 10/15/2020
# Week 1 Day 4 Program

# Goals:
# Today, I wanted to try out some of the other methods from
# the ImageOps module. I also wanted to start incorporating
# the other things I have learned, like enhancing brightness.

from PIL import Image, ImageOps, ImageEnhance

def main():
    image = Image.open("images/WRXProfile.jpg")
    image = image.rotate(270)
    image = image.reduce(2)
    image.save("images/RotatedProfile.jpg")

    # Posterization & enhanced brightness
    # The posterize method takes an image and how many
    # bits each color can have. The more bits, the closer
    # the image would be to the original. The fewer bits,
    # the more extreme the difference between colors will be
    imagePoster = ImageOps.posterize(image, 2)

    # Here I just thought the image would look nicer
    # if it was more bright
    enhancedPoster = ImageEnhance.Brightness(imagePoster)
    imagePoster = enhancedPoster.enhance(1.5)
    imagePoster.save("images/Posterization.jpg")

    # Inverted
    # This method only takes an image and inverts
    # each color value.
    imageInvert = ImageOps.invert(image)
    imageInvert.save("images/Inverted.jpg")

    # Solarization & enhanced brightness
    # This method takes an image and a threshold
    # It inverts every color value above the threshold
    # The default threshold is 128, but I liked 140 better
    imageSolar = ImageOps.solarize(image, 140)

    # Also thought this image would look nicer if it was brighter
    enhancedSolar = ImageEnhance.Brightness(imageSolar)
    imageSolar = enhancedSolar.enhance(1.75)
    imageSolar.save("images/Solarization.jpg")



if __name__ == "__main__":
    main()