# Charlie Honigman
# CSC 355
# 10/16/2020
# Week 1 Day 5 Program

# Goals:
# Use what I have learned about Pillow to make some cool images.

from PIL import Image, ImageEnhance, ImageFilter, ImageOps

def main():
    image = Image.open("images/LineUp.jpg")

    # The original image is very dreary, so I wanted to start by adding
    # more light, but not so much that the windows would get blown out
    # when I adjusted the saturation.
    enhancer = ImageEnhance.Brightness(image)
    brightened = enhancer.enhance(1.1)

    # The image was brighter, but the colors were still very cold, so
    # I used the color method of ImageEnhance to make them warmer.
    enhancer = ImageEnhance.Color(brightened)
    andColored = enhancer.enhance(3)

    # I wanted to incorporate another technique into this image, and thought
    # that I could take my goal of making the image less bleak even further by
    # overlaying a warm transparent mask over top. I ended up choosing pale
    # violet as the color for my mask because it was a nice warm color that
    # felt soft on the eyes due to its lower saturation.
    pinkMask = Image.new(size=andColored.size, color=(219, 112, 147, 128), mode="RGBA")
    andColored.paste(pinkMask, None, mask=pinkMask)

    # I was pretty pleased with this image, but felt that it looked just
    # a little too grainy. To fix this, I added a smooth filter to the image.
    finalImage = andColored.filter(ImageFilter.SMOOTH)

    finalImage.save("images/BrightenedAndMasked.jpg")

    # My next goal was to accomplish the same effect of making the image less
    # dreary, without enhancing its colors.
    # To do this, I started by switching the image to grayscale because it
    # remove the more drab colors of the first two cars.
    gray = ImageOps.grayscale(image)

    # Next, I upped the brightness of the image. I found that I could
    # adjust the brightness further here than earlier as I was not messing
    # with the saturation of the colors.
    enhancer = ImageEnhance.Brightness(gray)
    bright = enhancer.enhance(1.25)

    # Finally, I added a gaussian blur filter to the image to make the image
    # Just a touch softer without totally losing focus.
    outImage = bright.filter(ImageFilter.GaussianBlur)
    outImage.save("images/GrayscaledAndEnhanced.jpg")


if __name__ == "__main__":
    main()