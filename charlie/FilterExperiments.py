# Charlie Honigman
# CSC 355
# 10/14/2020

# Goals:
# Today, I wanted to play around with the predefined filters in
# the Image module. Some produced cool effects, others did not appear to
# change too much about the image. On friday, I may try combining some of
# these filters with what I've learned in the previous days

import numpy as np
from PIL import Image, ImageFilter


def main():
    image = Image.open("images/RotatedProfile.jpg")

    # To apply a filter you use to filter method on you Image object.
    # This method takes an ImageFilter object. If you are using predefined
    # filters, you use ".NAME_OF_FILTER"
    imageBlur = image.filter(ImageFilter.BLUR)
    imageBlur.save("images/Blur.jpg")

    imageCon = image.filter(ImageFilter.CONTOUR)
    imageCon.save("images/Contour.jpg")

    # Not sure what this one even does
    imageDet = image.filter(ImageFilter.DETAIL)
    imageDet.save("images/Detail.jpg")

    # Sharpens lines and edges
    imageEdgEnhance = image.filter(ImageFilter.EDGE_ENHANCE)
    imageEdgEnhance.save("images/EdgeEnhance.jpg")

    # Allegedly sharpens lines and edges even more, but
    # really only looks like it adds noise
    imageMoreEdg = image.filter(ImageFilter.EDGE_ENHANCE_MORE)
    imageMoreEdg.save("images/EdgeEnhanceMore.jpg")

    # Creates almost an imprint of the image
    imageEmboss = image.filter(ImageFilter.EMBOSS)
    imageEmboss.save("images/Emboss.jpg")

    # Lines are white, everything else is more or less black
    imageEdges = image.filter(ImageFilter.FIND_EDGES)
    imageEdges.save("images/FindEdges.jpg")

    # A more subtle edge enhancement
    imageSharpen = image.filter(ImageFilter.SHARPEN)
    imageSharpen.save("images/Sharpen.jpg")

    # Hard to tell what this one does
    imageSmooth = image.filter(ImageFilter.SMOOTH)
    imageSmooth.save("images/Smooth.jpg")

    # Unsurprisingly, even harder to tell what this one does
    imageSmoothMore = image.filter(ImageFilter.SMOOTH_MORE)
    imageSmoothMore.save("images/SmoothMore.jpg")

if __name__ == "__main__":
    main()

