# filter.py by Ben Soderberg
# CSC 355
# Work for Week 1
# October 16, 2020

'''
This program is a demostration of a selection of PIL's included filters.
The program will ask for which filter to use and will
display NightHut.jpg with the selected filter applied to it.
'''
from PIL import Image, ImageFilter
# Importing multiprocess to run my main function. Process will take
# the function and distribute the computation load to multiple cores
# therefore, making my program run faster. In my testing, I've seen times
# decrease by about 30% - 40% (although your milage may vary depending
# on what computer you have).
from multiprocess import Process

def main(filter):
    image = Image.open("NightHut.jpg")

    if filter == 1:
        grape = image.filter(ImageFilter.BLUR)
        grape.show()
    if filter == 2:
        grape = image.filter(ImageFilter.CONTOUR)
        grape.show()
    if filter == 3:
        grape = image.filter(ImageFilter.DETAIL)
        grape.show()
    if filter == 4:
        grape = image.filter(ImageFilter.EDGE_ENHANCE)
        grape.show()
    if filter == 5:
        grape = image.filter(ImageFilter.EDGE_ENHANCE_MORE)
        grape.show()
    if filter == 6:
        grape = image.filter(ImageFilter.EMBOSS)
        grape.show()
    if filter == 7:
        grape = image.filter(ImageFilter.FIND_EDGES)
        grape.show()
    if filter == 8:
        grape = image.filter(ImageFilter.SHARPEN)
        grape.show()
    if filter == 9:
        grape = image.filter(ImageFilter.SMOOTH)
        grape.show()
    if filter == 10:
        grape = image.filter(ImageFilter.SMOOTH_MORE)
        grape.show()
    else:
        print("Invalid Filter Number")

if __name__ == '__main__':
    print("Please enter the number for the filter you would like to use")
    print("1 : BLUR")
    print("2 : CONTOUR")
    print("3 : DETAIL")
    print("4 : EDGE_ENHANCE")
    print("5 : EDGE_ENHANCE_MORE")
    print("6 : EMBOSS")
    print("7 : FIND_EDGES")
    print("8 : SHARPEN")
    print("9 : SMOOTH")
    print("10 : SMOOTH_MORE")
    filter = int(input("Num: "))
    #print(filter)
    #print(type(filter))
    #print (filter == 1)
    p = Process(target=main, args=((filter,)))
    p.start()
    p.join()
