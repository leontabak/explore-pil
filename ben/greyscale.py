# greyscale.py by Ben Soderberg
# CSC 355
# Work for Week 1
# October 16, 2020

'''
This program takes the image NightHut.jpg and converts it to psudo greyscale.
I don't know why it does this. If you look at the code, you'd think
that this program would invert the color of the image (which is what
I was intending to create). But for some reason it produces a somewhat
greyscale image. I think it might have to do with some kind of file
type incompatiblity but I couldn't figure it out.

I say "somewhat greyscale" because I do not mean to say that it actually
converts to greyscale. If you zoom in on the resulting image, you can see
that some of the pixels still have color. Weird.
'''
# Importing PIL to do image stuff with and numpy to mess around with the
# pixels in the PIL image
from PIL import Image
import numpy as np
# Importing multiprocess to run my main function. Process will take
# the function and distribute the computation load to multiple cores
# therefore, making my program run faster. In my testing, I've seen times
# decrease by about 30% - 40% (although your milage may vary depending
# on what computer you have).
from multiprocess import Process

def main():
    im = np.array(Image.open("NightHut.jpg"))
    #print(im[0])
    for i in im:
        for j in i:
            for k in j:
                k = 255 - k
    #print(im[0])

    PIL_image = Image.fromarray(np.uint8(im)).convert('RGB')

    im.show()

if __name__ == '__main__':
    p = Process(target=main, args=())
    p.start()
    p.join()
