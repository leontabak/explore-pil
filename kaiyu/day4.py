# Kaiyu Wang
# day 4 2020\10\15
# Today, I just had an idea to mix my picture. To have 4 different effects in one picture.
# So I did a lot of research on typography. But I failed to do so.
# I decided to finish this idea in day 5.


# Import PIL modules
from PIL import Image, ImageFilter 

# Import sys module for sys.argv(to get filename)
import sys
# Get filename from argument
filename = sys.argv[1] 

# Open the image file
image = Image.open(filename) 

# ===================================================
# Add effect to filter
image = image.filter(ImageFilter.FIND_EDGES) 
# ===================================================

# Save the output image
image.save('output4.png')
