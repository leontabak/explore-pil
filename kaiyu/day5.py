# Kaiyu Wang
# day 5 2020/10/16
# I learn from the internet and ask friends to finish the mix picture.




# Import PIL modules
from PIL import Image, ImageFilter 

# Import sys module for sys.argv(to get filename)
import sys
# Get filename from argument
filename = sys.argv[1] 

# Open the image file
image = Image.open(filename) 
width, height = image.size[0], image.size[1] # Get image size
# Create a empty image for output
result = Image.new("RGBA",(width * 2, height * 2))

# ===================================================
# Add effects to filter
result.paste(image.filter(ImageFilter.FIND_EDGES), 				(width * 0, height * 0, width * 0 + width, height * 0 + height))
result.paste(image.filter(ImageFilter.GaussianBlur(10)), 		(width * 1, height * 0, width * 1 + width, height * 0 + height))
result.paste(image.filter(ImageFilter.EMBOSS), 					(width * 0, height * 1, width * 0 + width, height * 1 + height))
result.paste(image.filter(ImageFilter.CONTOUR), 				(width * 1, height * 1, width * 1 + width, height * 1 + height))
# ===================================================

# Save the output image
result.save('output5.png')
