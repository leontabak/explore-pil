# Kaiyu Wang
# day 1 2020/10/12
# Today I learned gaussian blur on the Internet.
# In image processing, a Gaussian blur (also known as Gaussian smoothing) is the result of blurring an image
# It is a widely used effect in graphics software,
# typically to reduce image noise and reduce detail.
# The visual effect of this blurring technique is a smooth blur resembling
# that of viewing the image through a translucent screen,
# distinctly different from the bokeh effect produced by an out-of-focus lens
# or the shadow of an object under usual illumination.



# Import PIL modules
from PIL import Image, ImageFilter 

# Import sys module for sys.argv(to get filename)
import sys
# Get filename from argument
filename = sys.argv[1] 

# Open the image file
image = Image.open(filename) 

# ===================================================
# Add effect to filter. THis is copied from Internet. The bigger parameter is, the more blurred the picture is.
# This parameter describes the radius of gaussian blur
image = image.filter(ImageFilter.GaussianBlur(radius=10))
# ===================================================

# Save the output image
image.save('output1.png')
