"""
explore-enhance.py

Author: Jakob Orel
Date: 10-13-20
CSC355 Open Source Development
Week 1

This program explores the different effects of ImageEnhance and
applys these effects in different slices. Use arguments in command
line when running program to select enhance effect and factor value.

"""
import numpy as np
import argparse
from PIL import Image, ImageEnhance

# Arguments when running program are required:
# Image: the filename/path of the image to enhance
# Enhance_type: Value to select the enhance effect ("Sharpness","Color",
#   "Contrast","Brightness")
# Factor: enhance factor value (1 returns original image)
parser = argparse.ArgumentParser()
parser.add_argument('image', type=str, help='Image to be enhanced')
parser.add_argument('enhance_type', type=str, help='Value to select type of enhance', default=0)
parser.add_argument('factor', type=float, help='Enhance factor value', default=1.0)

args = parser.parse_args()

def main(args):
    print("Now we are messing with ImageEnhance!")
    picture = Image.open(args.image)


    # All ImageEnhance functions use the enhance() method with a factor
    # parameter. A factor value of 1 will always return the original image.
    # A value > 1 will have a more intense effect. A value < 1 will
    # be more dull.
    if (args.enhance_type == "Sharpness"):
        # Adjusts image sharpness. 0 gives blurred image and > 1 gives
        # sharpened image
        enhancer = ImageEnhance.Sharpness(picture)
        altered_picture = enhancer.enhance(args.factor)

    elif (args.enhance_type == "Color"):
        # Adjusts color balance. 0 gives black and white and > 1 gives
        # more intense colors
        enhancer = ImageEnhance.Color(picture)
        altered_picture = enhancer.enhance(args.factor)

    elif (args.enhance_type == "Contrast"):
        # Adjusts image contrast. 0 gives a solid grey image and > 1 gives
        # more contrasted colors
        enhancer = ImageEnhance.Contrast(picture)
        altered_picture = enhancer.enhance(args.factor)

    elif (args.enhance_type == "Brightness"):
        # Adjusts image brightness. 0 gives a black image and > 1 gives
        # lighter image
        enhancer = ImageEnhance.Brightness(picture)
        altered_picture = enhancer.enhance(args.factor)
    else:
        altered_picture = picture

    altered_picture.show()

    #Now lets only do it on the left side
    width, height = picture.size
    # initialize array with zeros
    # specify the type of the elements as 8-bit unsigned integers
    mask_data = np.zeros( (height, width ), dtype = np.uint8 )

    # assign 255 to the all elements in the right half
    # of the array
    mask_data[ :, width // 2: ] = 255
    # covert data to mask image
    mask = Image.fromarray( mask_data, mode = "L" )

    # Use a composite to only have the effect on half the picture
    compositeImage = Image.composite(picture, altered_picture, mask)

    compositeImage.show()


if __name__ == "__main__":
    main(args)
