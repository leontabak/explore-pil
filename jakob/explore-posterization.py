"""
explore-posterization.py

Author: Jakob Orel
Date: 10-14-20
CSC355 Open Source Development
Week 1

This program explores posterizing (reducing the number of colors available)
in an image. I use a manual way to calculate the pixel values
and a function in ImageOps.

"""
import numpy as np
from PIL import Image, ImageOps

def main():
    print("Exploring Posterization")
    picture = Image.open("images/grand-canyon.png")

    # First start with Leon's proposition of posterizing a grayscale image
    grayscalePicture = picture.convert("L")

    width, height = grayscalePicture.size
    print(f"width = {width} height = {height}")
    #convert picture to array
    a = np.asarray(grayscalePicture)
    array = a.copy()

    # For every pixel, round the color value to the nearest multiple of factor
    # Height must go before width when using arrays. Different than images.
    for i in range(0, height):
        for j in range(0, width):
            array[i,j] = 64 * (array[i,j]//64)

    # convert to image and show
    grayscalePicture = Image.fromarray(array)
    grayscalePicture.show()

    # Try now with color image...
    width, height = picture.size

    # convert picture to array
    d = np.asarray(picture)
    print("mode = ", picture.mode)
    data = d.copy()
    print("Shape = ",data.shape)
    # For every pixel, round each RGB value to the nearest multiple of factor
    for i in range(0, height):
        for j in range(0, width):
            data[i,j,:] = 64 * (data[i,j,:]//64)

    # convert to image and show
    colorPicture = Image.fromarray(data)
    colorPicture.show()

    # ImageOps provides a posterize function to do this. Parameters are the
    # image and the number of bits to keep for each channel (1-8)
    # This is much faster than visiting each pixel and performing an operation.
    picture = ImageOps.posterize(picture, 2)
    picture.show()

if __name__ == "__main__":
    main()
