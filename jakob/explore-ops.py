"""
explore-ops.py

Author: Jakob Orel
Date: 10-14-20
CSC355 Open Source Development
Week 1

This program experiments with functions from the ImageOps module. It explores
the flip, mirror, solarize, posterize, autocontrast, expand, crop, scale,
equalize, pad, grayscale, and colorize functions to see the effects on an image.

I did not use argparse to pass arguments as many of these functions take
different types of arguments and are used for different purposes.
"""
from PIL import Image, ImageOps

def main():
    print("Exploring ImageOps")
    picture = Image.open("images/grand-canyon.png")
    picture.show()

    # Flip the image vertically (top to bottom)
    flipped = ImageOps.flip(picture)
    #flipped.show()

    # Flip image horizontally (left to right)
    mirrored = ImageOps.mirror(picture)
    #mirrored.show()

    # Invert all pixel values above a grayscale level threshold
    solarized = ImageOps.solarize(picture, threshold = 128)
    #solarized.show()

    # Reduce the number of bits (1-8) for each color channel.
    posterized = ImageOps.posterize(picture, bits = 2)
    #posterized.show()

    # Maximize (normalize) image contrast. This function calculates a histogram
    # of the input image (or mask region), removes cutoff percent of the
    # lightest and darkest pixels from the histogram, and remaps the image so
    # that the darkest pixel becomes black (0), and the lightest
    # becomes white (255). "cutoff" is the percent removed from histogram.
    # A higher cutoff percentage creates higher contrast. Ignore is the
    # background value.
    autoContrast = ImageOps.autocontrast(picture, cutoff=3, ignore=None)
    #autoContrast.show()

    # Adds border to image. Border is width, in pixels.
    # Print statements show how height and width are increased by 60.
    expanded = ImageOps.expand(picture, border=30, fill=(150,0,125))
    #expanded.show()
    print(f"Picture width= {picture.size[0]} Picture height= {picture.size[1]}")
    print(f"Expanded width= {expanded.size[0]} expanded height= {expanded.size[1]}")

    # Remove border from image. The same amount of pixels are removed from
    # all four sides. This function works on all image modes.
    # The Image module also provides the same crop function that takes a
    # box parameter.
    cropped = ImageOps.crop(expanded, border=25)
    #cropped.show()
    print(f"Cropped width= {cropped.size[0]} Cropped height= {cropped.size[1]}")

    # Returns a rescaled image by a specific factor given in parameter.
    # A factor greater than 1 expands the image, between 0 and 1 contracts
    # the image.
    scaled = ImageOps.scale(picture, 0.5)
    #scaled.show()

    # Equalize the image histogram. This function applies a non-linear mapping
    # to the input image, in order to create a uniform distribution of
    # grayscale values in the output image.
    equalized = ImageOps.equalize(picture)
    #equalized.show()

    # Returns a sized and padded version of the image, expanded to fill the
    # requested aspect ratio and size. Centering value of (0.5, 0.5) will keep
    # the image centered (0, 0) will keep the image aligned to the top left
    # (1, 1) will keep the image aligned to the bottom right. "fit" is a
    # similar function.
    padded = ImageOps.pad(picture, (512,512), centering=(0.5,0.5))
    #padded.show()

    # Converts image to grayscale
    grayscalePicture = ImageOps.grayscale(picture)

    # What happens if you equalize a grayscale image?
    equalizedGrayscale = ImageOps.equalize(grayscalePicture)
    #equalizedGrayscale.show()

    # Show that converting a grayscale picture to RGB does not colorize image
    convertedPicture = grayscalePicture.convert("RGB")
    #onvertedPicture.show()

    # Colorize grayscale image. This function calculates a color wedge which
    # maps all black pixels in the source image to the first color and all
    # white pixels to the second color. If mid is specified, it uses
    # three-color mapping. The black and white arguments should be RGB tuples
    # or color names; optionally you can use three-color mapping by also
    # specifying mid. Blackpoint, midpoint, and whitepoint can be manually
    # adjusted.
    colorized = ImageOps.colorize(grayscalePicture, black="orange", white="blue")
    colorized.show()

if __name__ == "__main__":
    main()
