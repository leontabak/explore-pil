"""
explore-grayscale.py

Author: Jakob Orel
Date: 10-12-20
CSC355 Open Source Development
Week 1

Used to explore and experiment with the converting color images to grayscale
and vice a versa. Uses Image.convert function and formulas to transform manually.

This program explores multiple different ways to transform an
RGB image to grayscale. The gradient functions were copied from
https://note.nkmk.me/en/python-numpy-generate-gradation-image/ and modified to
fit the example here.

"""

from PIL import Image, ImageOps, ImageDraw
import numpy as np

def get_gradation_2d(start, stop, width, height, is_horizontal):
    """Creates 2d gradient using np.tile and np.linspace.

    This function uses numpy.tile and numpy.linspace to distribute the values
    gradually. Linspace equally distributes values of an array. Tile arranges
    array vertically and horizontally. .T transposes the array to make it
    vertical. Returns ndarray after given start, stop, width, and height integers
    and is_horizontal bool.
    """
    if is_horizontal:
        return np.tile(np.linspace(start, stop, width), (height, 1))
    else:
        return np.tile(np.linspace(start, stop, height), (width, 1)).T

def get_gradation_3d(width, height, start_list, stop_list, is_horizontal_list):
    """Creates 3d gradient using get_gradation_2d.

    Uses get_gradation_2d in a for loop to do for 3d array. Integer values are
    passed for width, height, and three value tuples are passed for the start,
    stop, and horizontal lists. Returns ndarray.
    """
    #Initialize result array
    result = np.zeros((height, width, len(start_list)), dtype=np.float)

    for i, (start, stop, is_horizontal) in enumerate(zip(start_list, stop_list, is_horizontal_list)):
        result[:, :, i] = get_gradation_2d(start, stop, width, height, is_horizontal)

    return result

def gradient_composite(
        image1: Image, image2: Image) -> Image:
    """Generate a horizontal gradient using two images."""

    # Had to convert to RGBA because of transparency mask errors
    image1 = image1.convert("RGBA")
    image2 = image2.convert("RGBA")

    # Use the get_graduation_3d numpy function to create the gradient mask
    array = get_gradation_3d(512, 384, (0, 0, 0), (255, 255, 255), (True, True, True))
    # Make sure the mask is the same size as images and convert to "L"
    mask = Image.fromarray(np.uint8(array)).resize(image1.size).convert("L")

    return Image.composite(image1, image2, mask)

def main():
    print("Testing grayscale")
    grand_canyon = Image.open("images/grand-canyon.png")
    #reduce does not appear to do anything
    #forgot to store it in picture because it returns a copy of the image

    # storing the same image in a different variable so that I can keep
    # the original for future use
    picture = grand_canyon
    width, height = picture.size
    print(f"Width = {width} and height = {height}")
    grand_canyon = grand_canyon.reduce(2)
    picture = picture.reduce(2)
    width, height = picture.size
    # Print width and height to test reduce
    print(f"Width = {width} and height = {height} after reducing")

    # Inaccurate but just average RGB colors using numpy
    # Works but is not an accurate portrayal of greyscale values
    #imageArray = np.mean(picture, axis=2)
    #picture = Image.fromarray(imageArray)
    #picture.show()


    # Image.convert uses ITU-R 601-2 formula
    # (R * .299) + (G * .587) + (B * .114)
    # This is probably the easiest way to convert to grayscale
    #picture = picture.convert("L")

    # ImageOps.grayscale simply uses Image.convert("L")
    grayscalePicture = ImageOps.grayscale(picture)

    #convert image to array
    d = np.asarray(picture)
    data = d.copy()

    print("Data Shape: "+ str(data.shape))
    height, width, depth = data.shape

    #slice is the left half of picture
    slice = data[ : , 0 : (width // 2),:]
    print("Slice Shape: " + str(slice.shape))
    sliceShape = slice.shape

    # This does not appear to work correctly as the left half still has some
    # red color. Maybe the average is not calculated correctly?
    # Tried to do slice[:,:,0:2] but it gives error cannot broadcast shape
    # np.average is doing weighted average on the 3rd axis (RGB)
    # Attempted to use np.dot for dot product but had many issues with shape of
    # the arrays.
    # Had many issues getting the average color values assigned back to
    # the data array as discussed on a Piazza post.
    slice[:,:,0] = np.average(slice, axis=2, weights=[.299, .587, .114])
    slice[:,:,1] = np.average(slice, axis=2, weights=[.299, .587, .114])
    slice[:,:,2] = np.average(slice, axis=2, weights=[.299, .587, .114])
    data[:,0:(width//2)] = slice

    #convert array to image
    picture = Image.fromarray(data)

    #Use ImageDraw to draw a black line in the middle of picture
    draw = ImageDraw.Draw(picture)
    draw.line([(width//2,0),(width//2,height)], fill=(0,0,0), width=1)
    picture.show()


    #Passing grayscale and original picture to get a gradient blend
    # from grayscale to RGB
    gradientImage = gradient_composite(grand_canyon, grayscalePicture)
    gradientImage.show()

if __name__ == "__main__":
    main()
