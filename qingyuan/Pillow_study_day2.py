"Name:Qingyuan Liu"
"Date: 13/10/2020"
"Course: CSC355"
"Learn the Pillow function like ImageEnhance, ImageOps," \
"But it end up with learn how to upload my code to bitbucket" \
"And after many hours work, there is still some issuses that" \
"I cant fix, maybe i should ask for help."

import numpy as np
from PIL import Image
from PIL import ImageOps


def main():
    # open the image that going to use and make a smaller version of picture,
    # use ImageOps to change it to gray.then show how it looks like.
    image1 = Image.open("images/flowers-wall.jpg")
    imageSizeChange = image1.reduce(2)
    altered_picture = ImageOps.grayscale(imageSizeChange).convert("RGB")
    # altered_picture.show()

    # The following code is to cut the image to half. Half of the pciture will change to
    # white, then use it as mask composite to combine the orignal picture and gray picture together.
    width, height = altered_picture.size
    imageData = np.zeros((height, width), dtype=np.uint8)
    imageData[:, width // 2:] = 125
    mask = Image.fromarray(imageData, mode="L")
    half_and_half = Image.composite(imageSizeChange, altered_picture, mask)
    # half_and_half.show()

    # the following code is a little twist here, I use the image from top and keep work on it
    # and create a dark zone inside of the picture, There is no big change compare with original code.
    picture1 = np.array(half_and_half)
    height, width, depth = picture1.shape
    slice = picture1[(height // 4):(3 * height // 4), (width // 4):(3 * width // 4)]
    picture1[(height // 4):(3 * height // 4), (width // 4):(3 * width // 4)] = slice // 4
    finalPicture = Image.fromarray(picture1)
    finalPicture.show()
    finalPicture.save("images/day2work.jpg")


if __name__ == "__main__":
    main()
