Name: Qingyuan Liu
Date 16/10/2020
CourseL: CSC 355-2


The goal during this week is to study the Python package Pillow. To understand 
the function in the pillow that you can modify the pictures in different ways

In the first day, I start from very basic things like to change the color of a
picture by using ImageEnhance or modify some pixel's numbers. In second day, 
I changed the shape of a picture and learn how to combine two different style of
picture together. For the rest of the day, I had learn how to use the function from
ImageEnhance and ImageFilter. By using those two things, i create my final work.
To slice a picture to 6 parts and give them different style, then merge them back 
to one again. 

There is also some problems during the study, for example, set up the environment
or update some packages. Learn how to use git and upload my code. Most of them I have 
fixed, but there is still something i haven't find a solution. More details are in
the code. 
   
 
