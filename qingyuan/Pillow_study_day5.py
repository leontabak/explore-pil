"Name:Qingyuan Liu"
"Date: 16/10/2020"
"Course: CSC355"
"The last day's work, I am planing to cut a image to 6 parts and then" \
"use the function from pillow to do some changes, then join all image back together again" \
"And i think that will be cool."

import numpy as np
from PIL import ImageEnhance
from PIL import Image
from PIL import ImageFilter
from PIL.ImageFilter import CONTOUR, EMBOSS
import image_slicer

def main():
    print("hello world")

    # after hours of search, I have found a easy way to cut my image. There is
    # a package called image_slicer and it can cut image equally and
    # save to your current file by the numbers. But there is also a problem that you
    # can not control how the image cut, so it might become 2*3 or 3*2.
    image_slicer.slice("images/flowers-wall.jpg", 6)

    # the following codes are the modify for different parts of pictures.
    # The numbers that put inside of the code can be change. the higher number you
    # put in, the bigger change will show on picture, and the number 1.0 will show
    # the original pictures.
    picturePart1 = Image.open("images/flowers-wall_01_01.png")
    part1Change = ImageEnhance.Color(picturePart1).enhance(7.0)


    picturePart2 = Image.open("images/flowers-wall_01_02.png")
    part2Change = ImageEnhance.Contrast(picturePart2).enhance(5.0)


    picturePart3 = Image.open("images/flowers-wall_01_03.png")
    part3Change = ImageEnhance.Sharpness(picturePart3).enhance(10.0)


    picturepart4 = Image.open("images/flowers-wall_02_01.png")
    part4Change = ImageEnhance.Brightness(picturepart4).enhance(3.0)


    picturepart5 = Image.open("images/flowers-wall_02_02.png")
    part5Change = picturepart5.filter(ImageFilter.BLUR)


    picturepart6 = Image.open("images/flowers-wall_02_03.png")
    part6Change = picturepart6.filter(CONTOUR)


    # The next part of the code is to put all picture back to one again
    # here i use image.paste tp put them all together. And when the picture merge together,
    remerge = Image.new("RGB", (part1Change.width + part2Change.width + part3Change.width, part1Change.height + part4Change.height))
    remerge.paste(part1Change, (0, 0))
    remerge.paste(part2Change, (part1Change.width, 0))
    remerge.paste(part3Change, (part1Change.width + part2Change.width, 0))
    remerge.paste(part4Change, (0,part4Change.height))
    remerge.paste(part5Change, (part4Change.width, part4Change.height))
    remerge.paste(part6Change, (part5Change.width+ part6Change.width, part4Change.height))
    remerge.save("images/day5work.jpg")


if __name__ == "__main__":
    main()