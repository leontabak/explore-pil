"Name:Qingyuan Liu"
"Date: 15/10/2020"
"Course: CSC355"
"Today's work is about ImageFilter, the goal for following code is to try different kinds" \
"of function that ImageFilter have. And also, I am going to find a way to cut image to at least" \
" 4 part or more, then prepare for the last part of the code"

import numpy as np
from PIL import ImageEnhance
from PIL import Image
from PIL import ImageFilter
from PIL.ImageFilter import CONTOUR, EMBOSS


def main():
    print("hello world")

    # The first function that i tried is called ImageFilter.BLUR,
    # it applies the blur filter on an image.
    original = Image.open("images/flowers-wall.jpg")
    imageBLUR = original.filter(ImageFilter.BLUR)
    #imageBLUR.show()

    # The function in here is called CONTOUR, It add filter in to image.
    imageContour = original.filter(CONTOUR)
    imageContour.show()
    imageContour.save("images/day4work.jpg")


    # for following code, My goal is to cut the original picture to 8 parts
    # then give each part different modify by using pillow functions. The code below
    # is something that i found on stackoverflow, it do cut image to different parts.
    # But it does not give the picture that i want.

    chopsize = 300
    width, height = original.size

    for x0 in range(0, width, chopsize):
        for y0 in range(0, height, chopsize):
            box = (x0, y0,
                   x0 + chopsize if x0 + chopsize < width else width - 1,
                   y0 + chopsize if y0 + chopsize < height else height - 1)

            #original.crop(box).show()

    # I spend most of my time to look for way to cut the image, so i do no have
    # much picture work to show.

if __name__ == "__main__":
    main()
