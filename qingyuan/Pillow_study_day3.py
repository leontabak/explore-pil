"Name:Qingyuan Liu"
"Date: 14/10/2020"
"Course: CSC355"
"Finally, I pushed my code to bitbucket successfully. And I do not know" \
"how much vedio or website that i watched. But now, it all good now. " \
"Today's code is about the function ImageEnhance, I am going to create somehting " \
"fancy by using them...I hope"

import numpy as np
from PIL import ImageEnhance
from PIL import Image


def main():
    print("hello world")

    # the ImageEnhance.Color can be used to adjust the color balance of an image, the factor
    # 0.0 is giving the black and white picture and 1.0 will be the original picture.
    originalPicture = Image.open("images/flowers-wall.jpg")
    imageChange = ImageEnhance.Color(originalPicture)
    # imageChange.enhance(5.0).show()

    # The Contrast function is to control the image's contrast. the higher number you put in, the shadow
    # in the picture will be bigger.
    shadowChange = ImageEnhance.Contrast(originalPicture)
    # shadowChange.enhance(3.0).show()

    # The following code is doing the thing as their name showed. the ImageEnhance.Sharpness is to control the
    # sharpness of a image. Brightness is to control the bright of a picture. The index that used in side will be
    # same as they used in previous one: The higher number you put in, the bigger change shows in picture

    imageSharp = ImageEnhance.Sharpness(originalPicture)
    # imageSharp.enhance(2.0).show()

    imageBright = ImageEnhance.Brightness(originalPicture)
    # imageBright.enhance(4.0).show()

    # I combine yesterday's work into today's work, I cut my image to half and make
    # this two part modify by two function from ImageEnhance.

    imagepart1 = imageSharp.enhance(10.0)
    imagepart2 = imageChange.enhance(5.0)
    width, height = imagepart1.size
    imageData = np.zeros((height, width), dtype=np.uint8)
    imageData[:, width // 2:] = 255
    mask = Image.fromarray(imageData, mode="L")

    half_and_half = Image.composite(imagepart2, imagepart1, mask)
    half_and_half.show()
    half_and_half.save("images/day3work.jpg")


if __name__ == "__main__":
    main()
