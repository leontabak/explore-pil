# -*- coding: utf-8 -*-
"""
Created on Fri Oct   10:22:30 2020

@author: jackb
"""
from PIL import Image, ImageDraw
import argparse

'''
The purpose of this program is to use argparser to create a checkerboard mask of an image, 
then use it on another image as a composite. This process will create a checkerboarded 
pattern customizable by geometric shape and images. The first image will serve as the 
checkerboard and the second will be imposed upon. The third argument is --shape which 
can either be rectangle or ellipse. Rectangle will result in uniform squares and ellipse 
will scale squares based on distance from center of the image.
'''

from PIL import Image, ImageDraw
import argparse

def main(args):
    #Begin by loading in the args
    im1 = Image.open(args.image_file_name1)
    im2 = Image.open(args.image_file_name2)

    mask = Image.new("L", im1.size, 0)
    
    draw = ImageDraw.Draw(mask)
    
    shape = args.shape

    numRectangles = 50
    im = checkerboard(numRectangles, im1, im2, mask, draw, shape)
    im.show()


def impose_horizontal_bars(numRectangles, im1, im2, mask, draw, shape):
    shape = shape 
    start = 0
    stop = im2.size[1]
    Width = 10
    ystart = 0
    yend = Width
     
    for i in range(numRectangles):
         #Using ellipse will generate distroted bars with heights proportional to distance from center
         if shape == "ellipse":
             draw.ellipse((start, ystart, stop, yend), fill=255)
         elif shape == "rectangle":
             draw.rectangle((start , ystart, stop, yend), fill=255)
         im = Image.composite(im1, im2, mask)
         ystart, yend = ystart + 2 * Width, yend + 2 * Width
         
    return im

def impose_vertical_bars(numRectangles, im1, im2, mask, draw, shape):
    shape = shape 
    start = 0
    stop = im2.size[0]
    Width = 10
    xstart = 0
    xend = Width
    
    for i in range(numRectangles):
         #Note drawing ellipse here creates distorted bars with widths proportional to distance from center of image
         if shape == "ellipse":
             draw.ellipse((xstart, start, xend, stop), fill=255)
         elif shape == "rectangle":
             draw.rectangle((xstart , start, xend, stop), fill=255)
         im = Image.composite(im1, im2, mask)
         xstart, xend = xstart + 2 * Width, xend + 2 * Width
    
    return im

def checkerboard(numRectangles, im1, im2, mask, draw, shape):
    im = impose_horizontal_bars(numRectangles,im1,im2,mask, draw, shape)
    
    im = impose_vertical_bars(numRectangles,im,im1,mask, draw, shape)
    return im


if __name__ == "__main__":
    #Insert arg parser 
    parser = argparse.ArgumentParser(description='Produce checkerboard.')
    parser.add_argument('image_file_name1', type=str, help='an image for the drawing the checkerboard')
    parser.add_argument('image_file_name2', type=str, help='an image to be drawn onto')
    parser.add_argument('--shape', type=str, help='Choose between: rectangle and ellipse')

    args = parser.parse_args()
    
    main(args)