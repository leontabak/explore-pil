##Alex Heisdorffer
#Week 1 homework PIL library understanding 
#Project #4 I took an image and using the ImageDraw module drew
#a line that splits the image into two parts 
#October 16th 2020

from PIL import Image
from PIL import ImageDraw

#Opens my image that I am drawing a line through
photo = Image.open("image/20190908_151404_HDR.jpg")

#Draws a line throught the image with the desired color, width and where
#on the image the line will pass through 
draw = ImageDraw.Draw(photo)
draw.line((150,250, 175,325), fill=150, width=4)

photo.show()