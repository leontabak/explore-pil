#Alex Heisdorffer
#Week 1 homework PIL library understanding 
#Project #2 using grayscale to turn an image black and white 
#October 16th 2020

#In this project I am taking the same image that I used in the 1st project
#and using the ImageOps library to turn the image black and white

#One issue that I had with this program was that at first I believe that I was 
#looking at the wrong module to begin with so it was not working properly 
#because I had written some code that I did not need and it was printing 
#something completely different than what I was expecting 

from PIL import Image 
from PIL import ImageOps

photo = Image.open("image/20190908_151404_HDR.jpg")
photo.show()

#This part of the program is using the ImageOps library and the grayscale 
#function that is installed in the ImageOps module to take an image 
#that I have on file on my computer and turn it into black and white or
#more of just turing it to the color gray
gray_image = ImageOps.grayscale(photo)
gray_image.show()