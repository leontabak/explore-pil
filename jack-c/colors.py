'''
Jack Castiglione, Oct 14
Small Colors Library for use with other python files
'''

white = (255, 255, 255, 255)
black = (0, 0, 0, 255)
transparent = (255, 255, 255, 0)

red = (255, 0, 0, 255)
green = (0, 255, 0, 255)
blue = (0, 0, 255, 255)

yellow = (255, 255, 0, 255)
cyan = (0, 255, 255, 255)
magenta = (255, 0, 255, 255)

chartreuse = (128, 255, 0, 255)
