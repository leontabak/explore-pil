# Thu, 15 October 2020
# CSC355 Open Source Development
# Liam McCormick

# On Monday i had done a tiny bit with the ImageDraw class drawing text on top of an image
# to create a watermark effect, this is just playing ith drawing othe stuff, line and shapes

from PIL import Image, ImageDraw
#create bank canvas
im = Image.new('RGBA', (200, 200), 'white')

# get drawing context
draw = ImageDraw.Draw(im)

#draw a few assorted shapes
#draw a black line
draw.line([(0, 0), (199, 0), (199, 199), (0, 199), (0, 0)], fill='black')
#draw a blue rectangle
draw.rectangle((20, 30, 60, 60), fill='blue')
#draw a red circle
draw.ellipse((120, 30, 160, 60), fill='red')
#draw a weird shape to expieriment with the polygon
draw.polygon(((57, 87), (79, 62), (94, 85), (120, 90), (103, 113)),
   fill='brown')
#using a for loop to create a patern of green lines in the top right
for i in range(50, 200, 10):
    draw.line([(i, 0), (200, i - 50)], fill='green')
#sho image
im.show()