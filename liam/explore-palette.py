# Fri, 16 October 2020
# CSC355 Open Source Development
# Liam McCormick

# i came across the putpallet function and it looked like you could achieve something quite interesting with it
# I still don't fully understand it and am planning to work with it more over the weekend but
# i got this, it makes a very pleasing result in my opinion

from PIL import Image

# load the image to have colors converted
original = Image.open('chick.jpg').convert('RGB')

# use palette from reference image made below create the pallete, this part is modified from a
# stack overflow post
palette = [ 
    0,0,0,
    0,0,255,
    15,29,15,
    26,141,52,
    41,41,41,
    65,105,225,
    85,11,18,
    128,0,128,
    135,206,236,
    144,238,144,
    159,30,81,
    165,42,42,
    166,141,95,
    169,169,169,
    173,216,230,
    211,211,211,
    230,208,122,
    245,245,220,
    247,214,193,
    255,0,0,
    255,165,0,
    255,192,203,
    255,255,0,
    255,255,255
    ] + [0,] * 232 * 3

#turn pallete into an image
paletteImage = Image.new('P', (1, 1), 0)
# attahc pallete to image
paletteImage.putpalette(palette)


im = original.im.convert("P", 0, paletteImage.im)
# the 0 above means turn OFF dithering making solid colors, P is mode, and last arg is ima


# convert to RGB mode for display
imageCustomConvert = original._new(im).convert('RGB')
#show image
imageCustomConvert.show()