# Tue, 13 October 2020
# CSC355 Open Source Development
# Liam McCormick

# This one ended up being much simpler than i thought it would be
# the program get all image files in the curent directory and creates a thumbnail of them

import os
from PIL import Image

# Iterate through all the filenames in the ucrrent directory
for filename in os.listdir('.'):
    # check if file is in image
    if filename.endswith('png') or filename.endswith('.jpg'):
        # open file as image
        image = Image.open(filename)
        # Image.thumbnail is the optimal choice for this, it resisez the image in place such that the largest dimension
        # does not exceed the given value while preserving th aspect ratio. in this case it will mae it so the largest'
        # dimension of the photos is 200px
        image.thumbnail((200, 200))
        image.show()