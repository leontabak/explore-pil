# Mon, 12 October 2020
# CSC355 Open Source Development
# Liam McCormick

# I thought a good place to start might be trying to add a watermark to an image
# It seemed like a straight foraward enough project, i did end up making it slightly
# more complicated by tying fontsize to image size, and 

from PIL import Image, ImageDraw, ImageFont

# open image to be watermarked
image = Image.open("horse.jpg")

# get image size
image_width, image_height = image.size

# create an image for the watermark that is 1/4th of the height and 1/3rd the width of our image
# as it will be repeated in a 4x3 grid overlayed on top of the image
watermark_size = (image_width // 4, image_height // 3)
watermark = Image.new('RGBA', watermark_size, (255, 255, 255, 0))

# load arial font and set size to 1/40th of the images width
font = ImageFont.truetype("arial.ttf", image_width//40)
# get a drawing context
d = ImageDraw.Draw (watermark)

# set text for watermark
watermark_text = "Watermark"

# find coordinates to center the text in its grid
text_left = (watermark_size[0] - font.getsize(watermark_text)[0]) / 2
text_top = (watermark_size[1] - font.getsize(watermark_text)[1]) / 2

# draw text onto watermark in black at 50% opacity
d.text ((text_left, text_top), watermark_text, fill = (0, 0, 0, 50), font = font)

# rotate the watermark by 10 degrees
watermark = watermark.rotate(10,  expand=1)
watermark = watermark.resize(watermark_size, Image.ANTIALIAS)

# paste the watermark over the original image in a 4x3 grid
for i in range (0, image_width, watermark.size[0]):
    for j in range (0, image_height, watermark.size[1]):
        image.paste (watermark, (i, j), watermark)

image.show()
image.save("watermark.png")